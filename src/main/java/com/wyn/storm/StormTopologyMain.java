package com.wyn.storm;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class StormTopologyMain {

	private static final Logger LOG = LoggerFactory.getLogger(StormTopologyMain.class);

	public static final String TUPLE_GENERATOR_SPOUT = "GENERATOR_SPOUT";
	public static final String SOLR_BOLT = "SOLR_BOLT";

	@SuppressWarnings("serial")
	public static void main(String[] args) throws Exception {

		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout(TUPLE_GENERATOR_SPOUT, new GeneratorSpout(), 10);

		//builder.setBolt(PRINTER_BOLT, new PrintingBolt(), 1).shuffleGrouping(TUPLE_GENERATOR_SPOUT);
		builder.setBolt(SOLR_BOLT, new SolrBolt(), 10).shuffleGrouping(TUPLE_GENERATOR_SPOUT);

		Config conf = new Config();

		if (args.length > 0) {
			conf.setNumWorkers(10);
			StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
		} else {
			conf.setDebug(true);
			LocalCluster cluster = new LocalCluster();
			cluster.submitTopology("post-tuple-to-solr", conf, builder.createTopology());
			LOG.debug("READY!");
			while(true) 
                Utils.sleep(18000000); //sleep 5hrs
			// cluster.killTopology("post-tuple-to-solr");
			// cluster.shutdown();
		}
	}
}
