package com.wyn.storm;

import java.util.Map;

import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;

import java.util.Iterator;
import java.security.SecureRandom;
import java.math.BigInteger;
import java.util.UUID;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.security.SecureRandom;
import java.math.BigInteger;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class SolrBolt extends BaseRichBolt {

	private static final Logger LOG = LoggerFactory.getLogger(SolrBolt.class);

    private CloudSolrServer zkServer = null;
    private static String ZK_URL = "vsvphxslrdev01.hotelgroup.com:9095";
    private static String COLLECTION_NAME = "perf";

    OutputCollector _collector;

    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
        _collector = collector;
        
        try {
            connect(); // Connect to zk
        } catch(Exception ex) {
            LOG.error("Exception connecting to ZooKeeper: ", ex);
        }
    }

    int currentRecord = 1;
    public void execute(Tuple tuple) {
        // _collector.emit(tuple, new Values(tuple.getString(0) + "!!!"));

        SolrInputDocument document = new SolrInputDocument();

        Fields fields = tuple.getFields();
        Iterator<String> fieldsIterator = fields.iterator();
        while(fieldsIterator.hasNext()) {
            String fieldName = fieldsIterator.next();
            String fieldValue = "" + tuple.getValueByField(fieldName);

            document.addField(fieldName, fieldValue);
        }
        save(document);
        LOG.debug("Saved record # " + currentRecord);
        currentRecord++;

        _collector.ack(tuple);
    }

    public void cleanup() {
        disconnect(); // Disconnect from zk
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        // declarer.declare(new Fields("word"));
    }

    public Map getComponentConfiguration() {
        return null;
    }

    public void save(SolrInputDocument document) {
        LOG.debug("Inside: save().v.2.2");
        try {
            zkServer.add(document, 60000);
            LOG.debug("Document saved with id: " + document.getField("id"));
        } catch(Exception ex) {
            LOG.error("Exception in save()");
            ex.printStackTrace();
        }
        LOG.debug("Exiting: save().v.1.2");
    }

    public void connect() throws Exception {
        if(null==zkServer) {
            LOG.debug("Connecting to Zookeeper URL: [" + ZK_URL + "]");
            zkServer = new CloudSolrServer(ZK_URL);
            zkServer.connect();
            zkServer.setDefaultCollection(COLLECTION_NAME);
        } else {
            LOG.debug("Already connected to Zookeeper...");
        }
    }
    public void disconnect() {
        try {
            zkServer.shutdown();
        } catch(Exception ex) {
            LOG.error("Exception connecting to ZooKeeper: ", ex);
        }
    }
}
