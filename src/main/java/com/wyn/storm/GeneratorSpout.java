package com.wyn.storm;

import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import backtype.storm.tuple.Fields;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.Iterator;
import java.security.SecureRandom;
import java.math.BigInteger;
import java.util.UUID;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class GeneratorSpout extends BaseRichSpout {
	private static final Logger LOG = LoggerFactory.getLogger(GeneratorSpout.class);

	private SpoutOutputCollector collector;

	@SuppressWarnings("rawtypes")
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
	}

    boolean unlimited = false;
    int MAXRECORDS = 1000000;
    int currentRecord = 1;

	public void nextTuple() {

        try {
            if(unlimited || currentRecord<=MAXRECORDS) {
                LOG.debug("Generating tuple # " + currentRecord);

                // Values values = new Values("id-01", "Name-003");
                Values values = new Values(getUUID(), getRandomString(8), getRandomString(4), getRandomString(4), "RA", 
                                getRandomString(5), getRandomString(16), "2015-02-17T12:49:21Z", "NEW RESERVATION", 
                                getRandomString(2), "2015-02-17T12:49:21Z", 1, 0, getRandomString(2), 2, getRandomString(12), 
                                getRandomString(1), getRandomString(1), getRandomString(1), getRandomString(18), 
                                getRandomString(7), getRandomString(3), getRandomString(8), "25 " + getRandomString(7) + " Street", 
                                getRandomString(2), getRandomString(2), "USA", getRandomString(5), "SF" + getRandomString(1), 
                                getRandomString(3) + "1", getRandomString(3), "King Suite", 0, "Mart", "2015-02-17T12:49:21Z");

                this.collector.emit(values);
                currentRecord++;
            } else {
                LOG.debug("### Finished generating " + currentRecord + " records! ###");
                Utils.sleep(99999999); //Sleep 27hrs
            }
        } catch(Exception ex) {
            LOG.error("Exception in generating nextTuple()", ex);
            try { 
                Utils.sleep(5000); 
            } catch(Exception ex2) { 
                LOG.error("Exception in sleep", ex2); 
            }
        }
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("id", "GNR_NUM", "CONFIRMATION_NO", "CANCELLATION_NO", "BRAND_ID", "SITE_ID", 
                        "MEMBER_NUM", "BOOKING_TS", "RESV_STATUS", "CHANNEL", "CHECK_IN_DATE", "LOS", 
                        "POINTS_USED", "TOTAL_TAX", "NO_OF_ROOMS", "AWARD_NUMBER", "ADULTS", "CHILDREN", 
                        "CHILDREN_18", "GUEST_NAME", "GUEST_FIRST_NAME", "GUEST_MIDDLE_NAME", "GUEST_LAST_NAME", 
                        "GUEST_ADDRESS", "CITY", "STATE", "COUNTRY", "POSTAL_CODE", "RATE_PLAN", 
                        "ROOM_TYPE", "ROOM_REVENUE", "ROOM_TITLE", "RATE_SUPPRESSION_FLAG", "src", "ind_ts"));
	}

    public String getUUID() {
        return UUID.randomUUID().toString();
    }
    public String getRandomString(int len) {
        SecureRandom rs = new SecureRandom();
        int maxIterations=1000;
        String s="";
        int i=0;
        while(s.length()<len) {
            s+=(String) new BigInteger(230, rs).toString(len);
            i++;
        }
        return s.substring(0, len);
    }
    public String getRandomUrl() {
        String s = getRandomString(1950);
        return "http://www.google.com?data=" + s;
    }
}
